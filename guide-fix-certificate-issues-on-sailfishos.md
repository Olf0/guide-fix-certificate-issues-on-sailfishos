**This guide was primarily written for fixing root certificate issues on SailfishOS ≤ 4.0.1, specifically with the "DST Root CA X3" certificate distributed by Let's Encrypt.**

Note that this guide can be easily adapted, if similar issues with other root certificates occur.

BTW, [chapter B](#b-download-a-recent-ca-certificate-bundle-rpm-for-sailfishos-4) of this guide also contains a reminder how to download RPMs from other SailfishOS releases than the installed one.

### Documentation:
1. Basic issue description: https://letsencrypt.org/docs/dst-root-ca-x3-expiration-september-2021/
2. Detailed issue description: https://letsencrypt.org/docs/certificate-compatibility/#platforms-that-trust-dst-root-ca-x3-but-not-isrg-root-x1
   States "Jolla Sailfish OS > v1.1.2.16 (version that added ISRG Root X1 unknown)", i.e. [SailfishOS 1.1.2](https://together.jolla.com/question/82037/release-notes-112-yliaavanlampi/) which deployed [`ca-certificates-2014.2.1-1`](https://github.com/sailfishos/ca-certificates/blob/master/ca-certificates.changes#L16) containing [CKBI 1.97](https://github.com/sailfishos/ca-certificates/commit/f176bca9214888c2003ba4de1e1888f585950c18#diff-6ae47156be087fb9f71356c87412e2dd5ba9ee750f785a16cb3ce717d59c0663R298) from [NSS 3.16](https://wiki.mozilla.org/NSS:Versions) does include the ISRG Root X1 certificate, but see 6.
3. Technical details: https://letsencrypt.org/2020/12/21/extending-android-compatibility.html
4. Nice diagram depicting all this: https://letsencrypt.org/certificates/
5. Letsencrypt's production-chain change-log: https://community.letsencrypt.org/t/production-chain-changes/150739
6. Change-log of SailfishOS' ca-certificates RPM: https://github.com/sailfishos/ca-certificates/blob/master/ca-certificates.changes
   Likely version 2020.2.41-1 fixes this, rolled out in SailfishOS 4.1.0, 4.2.0 and 4.3.0 (search for "ca-certificates" in change-log from https://forum.sailfishos.org/t/changelog-kvarken-4-1-0/5938 )
   or version 2021.2.50-3 is going to fix this, which supposedly is going to be deployed by SailfishOS 4.4.0.
   Side note: As SailfishOS 4.3.0 switched from OpenSSL 1.0.2 to OpenSSL 1.1.x, it and any newer SailfishOS releases are not affected, regardless of this specific updated certificate bundle RPM.<br />
   Side note 2: Some [information from OpenSSL about the peculiarities of v1.0.2 WRT Let's encrypt certificates](https://www.openssl.org/blog/blog/2021/09/13/LetsEncryptRootCertExpire/).
7. Just to remind where this is originally coming from: https://github.com/sailfishos/ca-certificates/blob/master/ca-certificates.spec#L42
   I.e., "upstream" for SailfishOS is RedHat's Fedora distribution, which in turn retrieves and slightly adapts the bundle from Mozilla.


### Two approaches to handle this on old SailfishOS releases:

#### A. Specific to this case: Blacklist the "DST Root CA X3" certificate
   -  The correct basic assessment and almost the right implementation (patches the source bundle, not the target one): https://forum.sailfishos.org/t/storeman-and-side-loading-problems-also-root-ca-x3-certificate-expiration/8615/15
      If you have applied this guide, revert it per `devel-su mv /usr/share/pki/ca-bundle.trust.p11-kit.orig /usr/share/pki/ca-trust-source/ca-bundle.trust.p11-kit`  Never mind, if you fail to perform this reversion step (e.g. because you cannot find the backed up, original file), it is primarily a janitorial measure.  You may alternatively re-install the original source bundle per `devel-su pkcon --allow-reinstall install ca-certificates`
   -  Good, thorough analysis and advice, accompanied by a short and a lengthy implementation: https://blog.devgenius.io/rhel-centos-7-fix-for-lets-encrypt-change-8af2de587fe4
   -  Man page of the crucial tool to handle this: https://www.unix.com/man-page/centos/8/update-ca-trust/
   -  The certificate all this is about, which is not available at Let's encrypt and IdenTrust any more: https://crt.sh/?id=8395
   1. Become root:
      `devel-su`
   2. Change into the blacklist directory:
      `cd /etc/pki/ca-trust/source/blacklist`
   2a. [Optional] Backup the target bundle:
      `cp /etc/pki/tls/certs/ca-bundle.crt /tmp/ca-bundle.crt-backup`
   3. Download the offending certificate:
      `curl -kLo DST-RootCA-X3.pem https://crt.sh/?d=8395`
   3b. [Alternative to 3 / Optional, when 3 worked well] Locally extract the offending certificate and compare it with the downloaded one.
      `pkcon install openssl`
      `trust dump --filter "pkcs11:id=%c4%a7%b1%a4%7b%2c%71%fa%db%e1%4b%90%75%ff%c4%15%60%85%89%10" | openssl x509 > DST-Root-CA-X3.pem`
      `diff DST-RootCA-X3.pem DST-Root-CA-X3.pem`
      If diff did output nothing (i.e., the two files have identical content), all is fine, thus delete one of them:
      `rm DST-Root-CA-X3.pem`
   4. Create a new target bundle:
      `update-ca-trust extract`
   4a. [Optional, only feasible when 2a was performed] Verify that the offending certificate is no longer in the target bundle:
      `diff /tmp/ca-bundle.crt-backup /etc/pki/tls/certs/ca-bundle.crt`
      `cat DST-RootCA-X3.pem`
      The content between the "Begin" and "End" lines of the diff and the cat output must be the same, except for the `< `-prefix of the diff output; a visual check is sufficient.

#### B. Download a recent CA-certificate bundle RPM for SailfishOS, 
   **either per zypper (most elegant)**
    **or on a device with a newer SailfishOS release installed (see Documentation section 5)**
   **or by traversing Jolla's RPM repositories.**
   1. Become root:
      `devel-su`
   2. Install zypper:
      `pkcon install zypper`
   3. Add a newer SailfishOS release repository and name it arbitrarily:
      `zypper ar -c https://releases.jolla.com/releases/3.4.0.24/jolla/armv7hl foo`
      Syntax: `zypper addrepo -c https://releases.jolla.com/releases/$releasever/jolla/$arch $name`
   4. Test first!  For up(date) & in(stall) always use `-Dd` (`--dry-run --download`) together (see man-page why).
      Simply let the resolver pick the highest version (because all repos have the same priority) or use `--repo` to select a specific one; note that the recommended option `--from` (over `--repo`) is not available in older zypper versions.
      `zypper up -Dd ca-certificates`
      Rather do only temporarily accept the repository signing key (because of step 6).
      Aforementioned alternatives, not advantageous here: `#zypper up -Dd --repo foo ca-certificates  # Better, if available: zypper up -Dd --from foo ca-certificates`
   5. When you are sure what you want to do, do the real thing:
      `zypper up ca-certificates`
   6. Finally remove the temporarily added repository:
      `zypper rr -v foo`

**or**

   1. On the newer SailfishOS installation:
      `devel-su pkcon download /tmp ca-certificates`
      Choose "1", the "noarch" RPM (not the "source" one).
   2. Copy the downloaded RPM to target machine:
      `scp /tmp/ca-certificates-*.jolla.noarch.rpm nemo@<target-machine>:/tmp/`
   3. On the target machine (use file name completion per <Tab> key):
      `devel-su pkcon install-local /tmp/ca-certificates-XXXX.Y.ZZ-ABC.jolla.noarch.rpm`

**or**

   0. Prerequisite (optional): Determine the correct URL for the ca-certificates RPM (example for armv7hl, work the same way for aarch64 or i486, but this is a noarch RPM anyway):
      `cd /tmp`
      `curl -kL https://releases.jolla.com/releases/4.3.0.12/jolla/armv7hl/repodata/291c3ba847217d4db39b8bd73baee748409d77f7f06a63518cd6e3527b002ad7-primary.xml.gz | gunzip > repodata_sailfishos-4.3.0.12-armv7hl.xml`
      `less repodata_sailfishos-4.3.0.12-armv7hl.xml`
      Search in less per: `/<name>ca-certificates`
      Copy the URL part, which is specific for this RPM from the line: `<location href="oss/noarch/ca-certificates-2020.2.41-1.4.1.jolla.noarch.rpm"/>`
      Prepend the URL with `https://releases.jolla.com/releases/4.3.0.12/jolla/armv7hl/` (rsp. aarch64 or i486) to determine the full download URL:
      `https://releases.jolla.com/releases/4.3.0.12/jolla/armv7hl/oss/noarch/ca-certificates-2020.2.41-1.4.1.jolla.noarch.rpm`
      Note that ~~this is an academic example, because~~ this `ca-certificates` RPM specifies dependencies (for the first time, i.e. no former version did) on `p11-kit`, `p11-kit-nss-ckbi` and `p11-kit-trust`, ~~which cannot be satisfied on SailfishOS 2.x & 3.x.  I.e., I have not tried if force-installing `ca-certificates-2020.2.41` alone yields anything usable,~~ but all four packages together seem to be installable under e.g, SailfishOS 3.2.1, for example via the zypper method above.<br />
      See also the edit below: **\***
   1. Download the `ca-certificates` RPM per `curl`:
      `curl -kLO https://releases.jolla.com/releases/4.0.1.48/jolla/armv7hl/oss/noarch/ca-certificates-2018.2.26-1.2.1.jolla.noarch.rpm`
      This is the most recent release of the `ca-certificates` RPM, which still installs flawlessly on SailfishOS 2.x & 3.x.  Tested on SailfishOS 2.2.1 and 3.2.1, but the latter has exactly this RPM already installed by default (see `pkcon search name ca-certificates`).
   2. Install the downloaded RPM (use file name completion per \<Tab\> key):
      `devel-su pkcon install-local ca-certificates-XXXX.Y.ZZ-ABC.jolla.noarch.rpm`

#### Related threads at FSO
- https://forum.sailfishos.org/t/is-the-lets-encrypt-root-certificate-in-all-android-bundles/3507
- https://forum.sailfishos.org/t/storeman-and-side-loading-problems-also-root-ca-x3-certificate-expiration/8615/15
- https://forum.sailfishos.org/t/lets-encrypt-root-certificate-on-sailfishos-4-x/8419

<br />

P.S.: [Backup, text version and license](https://gitlab.com/Olf0/guide-fix-certificate-issues-on-sailfishos/-/tree/master)

<br />

**\*** *Edit:* `p11-kit`, `p11-kit-nss-ckbi` and `p11-kit-trust`, all at version 0.23.12, were already provided by SailfishOS 2.2.1 (and have not been updated at least up to SailfishOS 3.4.0), hence one may simply try installing the newest `ca-certificates` package per method B1.
If that fails due to some unmet dependency, on may use https://build.sailfishos.org/package/show/home:nephros:j1/ca-certificates
