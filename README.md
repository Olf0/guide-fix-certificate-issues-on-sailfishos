## Guide: Fix certificate issues on SailfishOS

**This guide was primarily written for fixing root certificate issues on SailfishOS ≤ 4.0.1, specifically with the "DST Root CA X3" certificate distributed by Let's Encrypt.**

Note that this guide can be easily adapted, if similar issues with other root certificates occur.

BTW, [chapter B](#b-download-a-recent-ca-certificate-bundle-rpm-for-sailfishos-4) of this guide also contains a reminder how to download RPMs from other SailfishOS releases than the installed one.

### Documentation:
1. Basic issue description: https://letsencrypt.org/docs/dst-root-ca-x3-expiration-september-2021/
2. Detailed issue description: https://letsencrypt.org/docs/certificate-compatibility/#platforms-that-trust-dst-root-ca-x3-but-not-isrg-root-x1<br />
   States "Jolla Sailfish OS > v1.1.2.16 (version that added ISRG Root X1 unknown)", i.e. [SailfishOS 1.1.2](https://together.jolla.com/question/82037/release-notes-112-yliaavanlampi/) which deployed [`ca-certificates-2014.2.1-1`](https://github.com/sailfishos/ca-certificates/blob/master/ca-certificates.changes#L16) containing [CKBI 1.97](https://github.com/sailfishos/ca-certificates/commit/f176bca9214888c2003ba4de1e1888f585950c18#diff-6ae47156be087fb9f71356c87412e2dd5ba9ee750f785a16cb3ce717d59c0663R298) from [NSS 3.16](https://wiki.mozilla.org/NSS:Versions) does include the ISRG Root X1 certificate, but see 6.
3. Technical details: https://letsencrypt.org/2020/12/21/extending-android-compatibility.html
4. Nice diagram depicting all this: https://letsencrypt.org/certificates/
5. Letsencrypt's production-chain change-log: https://community.letsencrypt.org/t/production-chain-changes/150739
6. Change-log of SailfishOS' ca-certificates RPM: https://github.com/sailfishos/ca-certificates/blob/master/ca-certificates.changes<br />
   Likely version 2020.2.41-1 fixes this, rolled out in SailfishOS 4.1.0, 4.2.0 and 4.3.0 (search for "ca-certificates" in change-log from [https://forum.sailfishos.org/t/changelog-kvarken-4-1-0/5938](https://forum.sailfishos.org/t/changelog-kvarken-4-1-0/5938?u=olf))<br />
   or version 2021.2.50-3 is going to fix this, which is deployed by SailfishOS 4.4.0.<br />
   Side note: As SailfishOS 4.3.0 switched from OpenSSL 1.0.2 to OpenSSL 1.1.x, it and any newer SailfishOS releases are not affected, regardless of this specific updated certificate bundle RPM.<br />
   Side note 2: Some [information from OpenSSL about the peculiarities of v1.0.2 WRT Let's encrypt certificates](https://www.openssl.org/blog/blog/2021/09/13/LetsEncryptRootCertExpire/).
7. Just to remind where this is originally coming from: https://github.com/sailfishos/ca-certificates/blob/master/ca-certificates.spec#L42<br />
   I.e. "upstream" for SailfishOS is RedHat's Fedora distribution, which in turn retrieves and slightly adapts the bundle from Mozilla.


### Two approaches to handle this on old SailfishOS releases:

#### A. Specific to this case: Blacklist the "DST Root CA X3" certificate
   -  The correct basic assessment and almost the right implementation (patches the source bundle, not the target one): [https://forum.sailfishos.org/t/storeman-and-side-loading-problems-also-root-ca-x3-certificate-expiration/8615/15](https://forum.sailfishos.org/t/storeman-and-side-loading-problems-also-root-ca-x3-certificate-expiration/8615/15?u=olf)<br />
      If you have applied this guide, revert it per `devel-su mv /usr/share/pki/ca-bundle.trust.p11-kit.orig /usr/share/pki/ca-trust-source/ca-bundle.trust.p11-kit`  Never mind, if you fail to perform this reversion step (e.g. because you cannot find the backed up, original file), it is primarily a janitorial measure.  You may alternatively re-install the original source bundle per `devel-su pkcon --allow-reinstall install ca-certificates`
   -  Good, thorough analysis and advice, accompanied by a short and a lengthy implementation: https://blog.devgenius.io/rhel-centos-7-fix-for-lets-encrypt-change-8af2de587fe4
   -  Man page of the crucial tool to handle this: https://www.unix.com/man-page/centos/8/update-ca-trust/
   -  The certificate all this is about, which is not available at Let's encrypt and IdenTrust any more: https://crt.sh/?id=8395
   1. Become root:<br />
      `devel-su`
   2. Change into the blacklist directory:<br />
      `cd /etc/pki/ca-trust/source/blacklist`<br />
   2a. [Optional] Backup the target bundle:<br />
      `cp /etc/pki/tls/certs/ca-bundle.crt /tmp/ca-bundle.crt-backup`
   3. Download the offending certificate:<br />
      `curl -kLo DST-RootCA-X3.pem https://crt.sh/?d=8395`<br />
   3b. [Alternative to 3 / Optional, when 3 worked well] Locally extract the offending certificate and compare it with the downloaded one.<br />
      `pkcon install openssl`<br />
      `trust dump --filter "pkcs11:id=%c4%a7%b1%a4%7b%2c%71%fa%db%e1%4b%90%75%ff%c4%15%60%85%89%10" | openssl x509 > DST-Root-CA-X3.pem`<br />
      `diff DST-RootCA-X3.pem DST-Root-CA-X3.pem`<br />
      If `diff` did output nothing (i.e. the two files have identical content), all is fine, thus delete one of them:<br />
      `rm DST-Root-CA-X3.pem`
   4. Create a new target bundle:<br />
      `update-ca-trust extract`<br />
   4a. [Optional, only feasible when 2a was performed] Verify that the offending certificate is no longer in the target bundle:<br />
      `diff /tmp/ca-bundle.crt-backup /etc/pki/tls/certs/ca-bundle.crt`<br />
      `cat DST-RootCA-X3.pem`<br />
      The content between the "Begin" and "End" lines of the diff and the cat output must be the same, except for the `< `-prefix of the diff output; a visual check is sufficient.

#### B. Download a recent CA-certificate bundle RPM for SailfishOS, 
   **either per zypper (most elegant and easy)** <br />
   **or on a device with a newer SailfishOS release installed (see Documentation section 5)** <br />
   **or by traversing one of Jolla's release repositories:**

**I.e. either**
   
   0. [Optional, see side note in step 4] Create a download repository and descend into it:<br />
      `mkdir sfos-rpms && cd sfos-rpms`
   1. Become root:<br />
      `devel-su`
   2. Install zypper:<br />
      `pkcon install zypper`
   3. Add a newer SailfishOS release repository and name it according to the release version:<br />
      `zypper ar -c https://releases.jolla.com/releases/4.4.0.72/jolla/armv7hl 4.4.0.72`<br />
      Syntax: `zypper addrepo -c https://releases.jolla.com/releases/$releasever/jolla/$arch $name`<br />
      Note that the `ca-certificates-2020.2.41` RPM file from `4.3.0.15` is known to work on SailfishOS ≥ 2.2.1; `4.4.0.72` and `4.5.0.25` provide a slightly newer version (`ca-certificates-2021.2.50`), and `4.6.0.15` a much newer one (`ca-certificates-2023.2.62`), which are all still untested (see also [the list of all SailfishOS releases](https://coderus.openrepos.net/whitesoft/sailversion)).
   4. Test first!  For up(date) & in(stall) always use `-Dd` (`--dry-run --download`) together (see man-page why).<br />
      Simply let the resolver pick the highest package version (because all repos have the same priority) or use `-r` (`--repo`) to select a specific one; note that the recommended option `--from` (over `--repo`) is not available in current zypper versions.<br />
      `zypper up -Dd ca-certificates`<br />
      Do only temporarily accept the repository signing key (because of step 6).<br />
      Aforementioned alternative, not advantageous here: `#zypper up -Ddr 4.4.0.72 ca-certificates`<br />
      Side note: You can *download* arbitrary RPM files, even for a different architecture and on a different computer by (it is a dry-run, choose "break dependencies" until it asks for downloading: choose `y`, but then `n` after the RPM file has been downloaded): `#zypper --pkg-cache-dir . in -DdRr 4.4.0.72 --force --no-recommends ca-certificates`
   5. When you are sure what you want to do, do the real thing:<br />
      `zypper up ca-certificates`
   6. Finally remove the temporarily added repository:<br />
      `zypper rr -v 4.4.0.72`
   
**or**

   1. On the newer SailfishOS installation:<br />
      `devel-su pkcon download /tmp ca-certificates`<br />
      Choose "1", the "noarch" RPM (not the "source" one).
   2. Copy the downloaded RPM to target machine (you may have to replace `nemo` by `defaultuser`):<br />
      `scp /tmp/ca-certificates-*.jolla.noarch.rpm nemo@<target-machine>:/tmp/`
   3. On the target machine (use file name completion per <Tab> key):<br />
      `devel-su pkcon install-local /tmp/ca-certificates-*.jolla.noarch.rpm`

**or**

   0. Prerequisite (optional): Determine the correct, URL for the ca-certificates RPM (example for armv7hl, works the same way for aarch64 or i486, but this is a noarch RPM anyway; but I have no idea how the hash valeue is generated):<br />
      `cd /tmp`<br />
      `curl -kL https://releases.jolla.com/releases/4.3.0.12/jolla/armv7hl/repodata/291c3ba847217d4db39b8bd73baee748409d77f7f06a63518cd6e3527b002ad7-primary.xml.gz | gunzip > repodata_sailfishos-4.3.0.12-armv7hl.xml`<br />
      `fgrep ca-certificates repodata_sailfishos-4.3.0.12-armv7hl.xml`<br />
      Copy the URL part, which is specific for this RPM from the line: `<location href="oss/noarch/ca-certificates-2020.2.41-1.4.1.jolla.noarch.rpm"/>`<br />
      Prepend the URL with `https://releases.jolla.com/releases/4.3.0.12/jolla/armv7hl/` (rsp. aarch64 or i486) to determine the full download URL:<br />
      `https://releases.jolla.com/releases/4.3.0.12/jolla/armv7hl/oss/noarch/ca-certificates-2020.2.41-1.4.1.jolla.noarch.rpm`<br />
      Note that ~~this is an academic example, because~~ this `ca-certificates` RPM specifies dependencies (for the first time, i.e. no former version did) on `p11-kit`, `p11-kit-nss-ckbi` and `p11-kit-trust`, ~~which cannot be satisfied on SailfishOS 2.x & 3.x.  I.e. I have not tried if force-installing `ca-certificates-2020.2.41` alone yields anything usable,~~ but all four packages together seem to be installable under e.g SailfishOS 3.2.1, for example via the zypper method above.<br />
      See also the edit below: **\***
   1. Download the `ca-certificates` RPM per `curl`:<br />
      `curl -kLO https://releases.jolla.com/releases/4.0.1.48/jolla/armv7hl/oss/noarch/ca-certificates-2018.2.26-1.2.1.jolla.noarch.rpm`<br />
      This is the most recent release of the `ca-certificates` RPM, which still installs flawlessly on SailfishOS 2.x & 3.x.  Tested on SailfishOS 2.2.1 and 3.2.1, but the latter has exactly this RPM already installed by default (see `pkcon search name ca-certificates`).
   2. Install the downloaded RPM (use file name completion per \<Tab\> key):<br />
      `devel-su pkcon install-local ca-certificates-XXXX.Y.ZZ-ABC.jolla.noarch.rpm`


#### Related threads at FSO
- [https://forum.sailfishos.org/t/is-the-lets-encrypt-root-certificate-in-all-android-bundles/3507](https://forum.sailfishos.org/t/is-the-lets-encrypt-root-certificate-in-all-android-bundles/3507?u=olf)
- [https://forum.sailfishos.org/t/storeman-and-side-loading-problems-also-root-ca-x3-certificate-expiration/8615/15](https://forum.sailfishos.org/t/storeman-and-side-loading-problems-also-root-ca-x3-certificate-expiration/8615/15?u=olf)
- [https://forum.sailfishos.org/t/lets-encrypt-root-certificate-on-sailfishos-4-x/8419](https://forum.sailfishos.org/t/lets-encrypt-root-certificate-on-sailfishos-4-x/8419?u=olf)

<br />

P.S.:
- This guide as [published at FSO](https://forum.sailfishos.org/t/fix-certificate-issues-on-sailfishos/8910?u=olf) is outdated, because it became immutable.
- [Backup, text version and license at GitLab](https://gitlab.com/Olf0/guide-fix-certificate-issues-on-sailfishos)

<br />

**\*** *Edit:* `p11-kit`, `p11-kit-nss-ckbi` and `p11-kit-trust`, all at version 0.23.12, were already provided by SailfishOS 2.2.1 (and have not been updated at least up to SailfishOS 3.4.0), hence one may simply try installing the newest `ca-certificates` package per method B1.
If that fails due to some unmet dependency, one may use https://build.sailfishos.org/package/show/home:nephros:j1/ca-certificates

--------------------------------------------
## Remarks

One shall not alter the source bundle!<br />
It belongs to an RPM and any update of that RPM (`ca-certificates`) will overwrite your changes in the source bundle, plus its `%post` script will trigger `update-ca-trust` , which overwrites the changes in the target bundle: then your device is back at the start (WRT the certificate issues).
For example, all this will happen during the next SailfishOS upgrade; I would not be surprised, if it fails to finish, due to failing https connections.

Do read the [update-ca-trust(8) man page](https://www.unix.com/man-page/centos/8/update-ca-trust/), which additionally explains why manual adaptions by a sysadmin usually should be performed in the `/etc/pki` hierarchy (priority over `/usr/share/pki` etc.).

--------------------------------------------
## Addendum

Please mind, that you nowadays also need OpenSSL 1.1 for “the WWW to work”, hence on SailfishOS < 4.0.1 one must:

Install a newer OpenSSL package from here: [OpenSSL 1.1.1 + 1.0.2 | OpenRepos.net](https://openrepos.net/content/openssl111102/openssl-111-102)

If you are doing this manually (without Storeman), you also have to download the OpenSSL ***-libs*** RPM (omit the *-devel* RPM): [openssl-libs 1.1.1 + 1.0.2 | OpenRepos.net](https://openrepos.net/content/openssl111102/openssl-libs-111-102)

When using the command line, you have to install these in one go (using the ARMv7 RPMs for, e.g. a Jolla 1 in this example):<br />
**devel-su&nbsp; pkcon&nbsp; install-local&nbsp; [openssl-1.1.1kgit1-1.7.4.jolla_.armv7hl.rpm](https://openrepos.net/sites/default/files/packages/17011/openssl-1.1.1kgit1-1.7.4.jolla_.armv7hl.rpm)&nbsp; [openssl-libs-1.1.1kgit1-1.7.4.jolla_.armv7hl.rpm](https://openrepos.net/sites/default/files/packages/17011/openssl-libs-1.1.1kgit1-1.7.4.jolla_.armv7hl.rpm)**
